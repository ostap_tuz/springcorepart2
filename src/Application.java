import beans1.BeanA;
import beans1.BeanB;
import beans1.BeanC;
import beans2.NarcissusFlower;
import beans2.RoseFlower;
import beans3.BeanD;
import beans3.BeanF;
import configs.*;
import fruits.AllFruits;
import fruits.GeneralFruits;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import otherBeans.OtherBeanA;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;
import transport.MyCar;
import transport.Van;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigForFisrtBean.class,
                ConfigForSecondAndThirdBean.class, ConfigForOthers.class, ConfigFruits.class);
        context.getBean(BeanA.class);
        context.getBean(BeanB.class);
        context.getBean(BeanC.class);
        context.getBean(BeanD.class);
        //context.getBean(BeanE.class);
        context.getBean(BeanF.class);
        //context.getBean(CatAnimal.class);
        context.getBean(NarcissusFlower.class);
        context.getBean(RoseFlower.class);

        System.out.println("---------------------");

        OtherBeanA otherBeanA  = context.getBean(OtherBeanA.class);
        OtherBeanB otherBeanB  = context.getBean(OtherBeanB.class);
        OtherBeanC otherBeanC  = context.getBean(OtherBeanC.class);
        System.out.println(otherBeanA+"\n"+otherBeanB+"\n"+otherBeanC);

        System.out.println("---------------------\nFruit order:");
        context.getBean(AllFruits.class).printFruits();
        System.out.println("---------------------");
        System.out.println(context.getBean(GeneralFruits.class));

        System.out.println("---------------------\nSingleton beans:");
        System.out.println("OtherBeanA -> "+context.getBean(OtherBeanA.class).hashCode());
        System.out.println("OtherBeanA -> "+context.getBean(OtherBeanA.class).hashCode());
        System.out.println("Prototype beans:");
        System.out.println("OtherBeanB -> "+context.getBean(OtherBeanB.class).hashCode());
        System.out.println("OtherBeanB -> "+context.getBean(OtherBeanB.class).hashCode());

        AnnotationConfigApplicationContext profileContext = new AnnotationConfigApplicationContext();
        profileContext.getEnvironment().setActiveProfiles("city");
        profileContext.register(ConfigTransport.class);
        profileContext.refresh();
        System.out.println("---------------------");
        System.out.println(profileContext.getBean(MyCar.class));
        System.out.println(profileContext.getBean(Van.class));

        AnnotationConfigApplicationContext profileContextVillage = new AnnotationConfigApplicationContext();
        profileContextVillage.getEnvironment().setActiveProfiles("village");
        profileContextVillage.register(ConfigTransport.class);
        profileContextVillage.refresh();
        System.out.println("---------------------");
        System.out.println(profileContextVillage.getBean(MyCar.class));
        System.out.println(profileContextVillage.getBean(Van.class));

    }
}
