package otherBeans;

import beans1.BeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanC {
    @Autowired
    private BeanC beanC;

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "beanC=" + beanC +
                '}';
    }
}
