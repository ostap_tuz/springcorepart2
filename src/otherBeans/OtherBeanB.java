package otherBeans;

import beans1.BeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanB {
    private BeanB beanB;

    @Autowired
    @Qualifier("beanB")
    public void setBeanB(BeanB beanB){
        this.beanB = beanB;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "beanB=" + beanB +
                '}';
    }
}
