package configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import transport.CityCar;
import transport.MyCar;
import transport.Van;

@Configuration
@ComponentScan("transport")
public class ConfigTransport {
    @Autowired
    private MyCar myCar;

    @Bean
    @Profile("city")
    public Van getCityVan(){
        Van van = new Van();
        van.setMark("City van");
        van.setModel("City model");
        return van;
    }

    @Bean
    @Profile("village")
    public Van getVillageVan(){
        Van van = new Van();
        van.setMark("Village van");
        van.setModel("Village model");
        return van;
    }

}
