package configs;

import beans2.CatAnimal;
import beans2.NarcissusFlower;
import beans2.RoseFlower;
import beans3.BeanD;
import beans3.BeanE;
import beans3.BeanF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "beans3", useDefaultFilters = false,
            includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanD.class, BeanF.class}),
            excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class)),
        @ComponentScan(basePackages = "beans2", useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "beans2\\.\\w*Flower"),
                excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CatAnimal.class)) })
public class ConfigForSecondAndThirdBean {}
