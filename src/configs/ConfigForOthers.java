package configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import otherBeans.OtherBeanA;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;

@Configuration
@ComponentScan(value = "otherBeans")
public class ConfigForOthers {}
