package beans3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String name;

    @Value("BEAN_D")
    public void setName(String name) {
        this.name = name;
        System.out.println("BeanD is created");
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                '}';
    }
}
