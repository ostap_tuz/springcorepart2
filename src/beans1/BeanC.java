package beans1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    private String name;

    @Value("BEAN_C")
    public void setName(String name) {
        this.name = name;
        System.out.println("BeanC is created");
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                '}';
    }
}
