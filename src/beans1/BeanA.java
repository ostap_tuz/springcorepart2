package beans1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class BeanA {
    private String name;

    @Value("BEAN_A")
    public void setName(String name) {
        this.name = name;
        System.out.println("BeanA is created");
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                '}';
    }
}
