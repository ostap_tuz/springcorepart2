package beans1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String name;

    @Value("BEAN_B")
    public void setName(String name) {
        this.name = name;
        System.out.println("BeanB is created");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                '}';
    }
}
