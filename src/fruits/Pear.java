package fruits;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class Pear implements IFruits {
    @Override
    public String getFruit() {
        return "Pear";
    }
}
