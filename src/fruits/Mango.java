package fruits;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class Mango implements IFruits {
    @Override
    public String getFruit(){
        return "Mango";
    }
}
