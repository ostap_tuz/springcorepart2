package fruits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class AllFruits {
    @Autowired
    private List<IFruits> fruits;

    public void printFruits(){
        for (IFruits fruit : fruits) {
            System.out.println(fruit.getFruit());
        }
    }
}
