package fruits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GeneralFruits {
    @Autowired
    private IFruits fruit1;

    @Autowired
    @Qualifier("pear")
    private IFruits fruit2;

    @Autowired
    @Qualifier("cherri")
    private IFruits fruit3;

    @Autowired
    @Qualifier("mango")
    private IFruits fruit4;

    @Override
    public String toString() {
        return "GeneralFruits{" +
                "fruit1=" + fruit1.getFruit() +
                ", fruit2=" + fruit2.getFruit() +
                ", fruit3=" + fruit3.getFruit() +
                ", fruit4=" + fruit4.getFruit() +
                '}';
    }
}
