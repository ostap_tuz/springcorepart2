package fruits;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public class Cherri implements IFruits {
    @Override
    public String getFruit() {
        return "Cherri";
    }
}
