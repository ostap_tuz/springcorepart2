package fruits;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Primary
@Order(1)
public class Apple implements IFruits{
    @Override
    public String getFruit() {
        return "Apple";
    }


}
