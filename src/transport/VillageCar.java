package transport;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("village")
public class VillageCar extends Car{
    private String mark;
    private String model;

    @Value("Mitsubish")
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Value("Pajero")
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "VillageCar{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
