package transport;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("city")
public class CityCar extends Car{
    private String mark;
    private String model;

    @Value("Mercedes")
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Value("Vito")
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "CityCar{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
