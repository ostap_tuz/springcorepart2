package transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyCar {
    @Autowired
    private Car car;

    @Override
    public String toString() {
        return "MyCar{" +
                "car=" + car +
                '}';
    }
}
