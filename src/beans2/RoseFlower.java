package beans2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private String name;

    @Value("ROSE")
    public void setName(String name) {
        this.name = name;
        System.out.println("Rose is created");
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name='" + name + '\'' +
                '}';
    }
}
