package beans2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String name;

    @Value("CAT")
    public void setName(String name) {
        this.name = name;
        System.out.println("Cat is created");
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "name='" + name + '\'' +
                '}';
    }
}
