package beans2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    private String name;

    @Value("NARCISSUS")
    public void setName(String name) {
        this.name = name;
        System.out.println("Narcissus is created");
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name='" + name + '\'' +
                '}';
    }
}
